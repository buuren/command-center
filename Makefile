# Project settings
ANSIBLE_PLAYBOOK := ansible-playbook
TASKS_PATH := tasks
VARS_PATH := vars
INFRA_DIR := /home/vlad/infra
INVENTORY := inventory.ini
LOCAL := $(TASKS_PATH)/local.yml
CENTOS7 := $(TASKS_PATH)/centos7.yml
CENTOS7_PLAYBOOK := $(INFRA_DIR)/centos7-template/ansible/playbook.yml

# ---------------- General tasks ----------------  : ## ----------------

enter: dev-tools-image/build ## Spawns a local dev environment in a docker image #docker system prune -a --volumes
	/usr/bin/docker run \
		--name dev-tools \
		--hostname=dev-tools \
		--entrypoint /bin/bash \
		--net=host \
		--privileged \
		-it \
		-v $(INFRA_DIR):$(INFRA_DIR) \
		-e USER=root \
		--rm \
		dev-tools:latest -c "bash"\

quit: chown ## Spawns a local dev environment in a docker image
	exit

chown: ## Spawns a local dev environment in a docker image
	@chown 1000:1000 $(INFRA_DIR) -R

dev-tools-image/build: ## Spawns a local dev environment in a docker image
	cd $(INFRA_DIR)/dev-tools && /usr/bin/docker build \
		--tag=dev-tools:latest \
		--build-arg PACKER=1.2.0 \
		--build-arg TERRAFORM=0.11.3 \
		--build-arg VAGRANT=2.0.2 \
		--build-arg AWSCLI=1.14.57 \
		--build-arg GO=1.10 \
		--build-arg SERVERSPEC=2.41.3 \
		.

# ---------------- CentOS 7 template tasks ----------------  : ## ----------------

centos7/build: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(LOCAL) -i $(INVENTORY) --extra-vars "task=$(MAKECMDGOALS).yml"

centos7/release: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(LOCAL) -i $(INVENTORY) --extra-vars "task=$(MAKECMDGOALS).yml"

centos7/test:  ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(LOCAL) -i $(INVENTORY) --extra-vars "task=centos7/vagrant-up.yml"
	${ANSIBLE_PLAYBOOK} $(CENTOS7) -i $(INVENTORY) --extra-vars "task=$(MAKECMDGOALS).yml"

centos7/vagrant-init: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(LOCAL) -i $(INVENTORY) --extra-vars "task=$(MAKECMDGOALS).yml"

centos7/vagrant-up: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(LOCAL) -i $(INVENTORY) --extra-vars "task=$(MAKECMDGOALS).yml"

# ---------------- Ansible tasks ----------------  : ## ----------------

ansible-centos7-local: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(CENTOS7_PLAYBOOK) -i $(INVENTORY) --extra-vars "host=$(MAKECMDGOALS)"

ansible-centos7-remote: ## Spawns a local dev environment in a docker image
	${ANSIBLE_PLAYBOOK} $(CENTOS7_PLAYBOOK) -i $(INVENTORY) --extra-vars "host=$(MAKECMDGOALS)"

.PHONY: help
help:
	@echo -e "\nExecute ... \n"
	@echo "System tasks:"
	@grep ' ## ' $(MAKEFILE_LIST) | grep -v '@grep' | awk '{split($$0,a,": "); printf("\t%s\n",a[1])}'


.DEFAULT_GOAL := help

